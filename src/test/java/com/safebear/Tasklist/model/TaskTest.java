package com.safebear.Tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;

public class TaskTest {
    @Test
    public void creation(){
        LocalDate localDate = LocalDate.now();
        Task task = new Task(1L,"Configure Jenkins Sever",localDate,false);
        Assertions.assertThat(task.getId()).isEqualTo(1L);
    }
}
