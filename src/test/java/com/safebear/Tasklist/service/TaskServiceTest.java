package com.safebear.Tasklist.service;

import com.safebear.Tasklist.model.Task;
import com.safebear.Tasklist.repository.TaskRepository;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

@RunWith(SpringRunner.class)
public class TaskServiceTest {

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    TaskService taskService;

    @Configuration
    static class TaskServiceTestContextConfiguration {

        @Bean
        public TaskRepository taskRepository() {
            return Mockito.mock(TaskRepository.class);
        }

        @Bean
        public TaskService taskService() {
            return new TaskServiceImpl();
        }

    }

    LocalDate localDate = LocalDate.now();
    private Task task = new Task(1L, "ConfigureJenkins", localDate, false);

    @Before
    public void setUp() {

        Mockito.when(this.taskRepository.findAll())
                .thenReturn(Lists.newArrayList(task));

        Mockito.when(this.taskRepository.save(task))
                .thenReturn(task);
    }

    @Test
    public void testGetList() {
        Iterable<Task> taskList = taskService.list();
        Assertions.assertThat(taskList.iterator().next().getId()).isEqualTo(1L);

    }

    @Test
    public void testSaveTask() {

        Assertions.assertThat(taskService.save(task).getId()).isEqualTo(1L);


    }
 }
