package com.safebear.Tasklist.usertests;

import com.safebear.Tasklist.usertests.pages.TaskListPage;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class StepDefs {
    //geting the url details
    final String DOMAIN = System.getProperty("domain");
    final String PORT = System.getProperty("port");
    final String CONTEXT = System.getProperty("context");

    WebDriver driver;
    TaskListPage taskListPage;

    @Before
    public void setUp(){
        String browser = System.getProperty("browser");

        switch (browser){
            case "headless":
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless");
                driver = new ChromeDriver(options);
                break;

            case "chrome" :
                driver = new ChromeDriver();
                break;9

            default:
                driver = new ChromeDriver();
                break;
        }
        taskListPage = new TaskListPage(driver);
        String url = DOMAIN + ":" + PORT + "/" + CONTEXT;
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown(){


        try {
            Thread.sleep(2000);
        }
        catch(InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }




    @When("^a user creates a (.+)$")
    public void a_user_creates_a_task(String taskname) throws Throwable {
        taskListPage.addTask(taskname);
        //throw new PendingException();
    }

    @Then("^the (.+) appears in the list$")
    public void the_task_appears_in_the_list(String taskname) throws Throwable {
        Assertions.assertThat(taskListPage.checkForTask(taskname)).isTrue();
       //throw new PendingException();
    }
//    @Given("^the following tasks are createed:$")
//    public void the_following_tasks_are_created(DataTable arg1) throws Throwable {
//        // Write code here that turns the phrase above into concrete actions
//        // For automatic transformation, change DataTable to one of
//        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
//        // E,K,V must be a scalar (String, Integer, Date, enum etc)
//        throw new PendingException();
//    }
}
