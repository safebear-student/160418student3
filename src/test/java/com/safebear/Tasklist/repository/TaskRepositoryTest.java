package com.safebear.Tasklist.repository;

import com.safebear.Tasklist.model.Task;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

@DataJpaTest
@RunWith(SpringRunner.class)

public class TaskRepositoryTest {

    @Autowired
    private TaskRepository repo;
    @Test
    public void findByName(){
        LocalDate localDate = LocalDate.now();
        this.repo.save(new Task(null,"Configure Jenkins",localDate,false));
        List<Task> tasklist = this.repo.findByName("Configure Jenkins");
        Assertions.assertThat(tasklist.size()).isEqualTo(1);

    }
}
