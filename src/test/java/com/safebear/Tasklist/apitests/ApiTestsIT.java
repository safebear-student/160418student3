package com.safebear.Tasklist.apitests;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import org.junit.Before;
import org.junit.Test;
import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class ApiTestsIT {
    final String DOMAIN = System.getProperty("domain");
    final int PORT = Integer.parseInt(System.getProperty("port"));
    final String CONTEXT = System.getProperty("context");

    @Before
    public void setUp(){

        RestAssured.baseURI=DOMAIN;
        RestAssured.port=PORT;
        RestAssured.registerParser("application/json", Parser.JSON);
    }

    @Test
    public void testHomepage(){
        get("/"+ CONTEXT + "/api/tasks")
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testSaveTask(){
         given()
                .contentType("application/json")
                .body("{\"name\":\"Configure Jenkins\",\"completed\":false,\"dueDate\":\"05/04/2018\"}").
                 when()
                 .post("/" + CONTEXT + "/api/tasks/save").
                 then()
                 .body("name",equalTo("Configure Jenkins"));
    }
}
