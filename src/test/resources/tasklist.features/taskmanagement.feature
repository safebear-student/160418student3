Feature: Task Management

  UserStory:
  In order to manage my tasks
  As a user
  I must be able to view, add, archive or update tasks.

  Rules:
  -You must be able to view all tasks
  -They must be paginated
  -You must be able to see the name, duedate and status of a task
  -You must be able to change the status of a task
  -You must be able to archive tasks

  Questions:
  -How many tasks per page for the pagination
  -Where do you archive to?
  -Is the due date configurable or should it be hard-coded to a certain number of days in the future.

  To do
  -Pagination
  -Archiving

  Domain Language
  -Task = this is made of a description, a status and a due date
  -Due Date = this must be in the format dd/MM/YYYY
  -Status = this is 'completed' or 'not completed'
  -Task List = this is a list of tasks and can be displayed on the home page


#  Background:
#    Given the following tasks are createed:
#    |Configure Jenkins|
#    |Set up automation environment|
#    |Set up SQL server            |


#  @to-do
  Scenario Outline: A user creates a task
    When a user creates a <task>
    Then the <task> appears in the list
    Examples:
    |task|
    |Create Documentation|