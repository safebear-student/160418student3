package com.safebear.Tasklist.repository;

import com.safebear.Tasklist.model.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskRepository extends CrudRepository<Task,Long> {

    List<Task> findByName(String name);
}
