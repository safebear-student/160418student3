package com.safebear.Tasklist.service;

import com.safebear.Tasklist.model.Task;

public interface TaskService {

    Iterable<Task> list();

    Task save(Task task);
}
