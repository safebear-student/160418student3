pipeline{
    agent any
    parameters{
        //tests to run API
        string(name:'apiTests',defaultValue: 'ApiTestsIT', description: 'API tests')
        //Cucumber
        string(name:'cuke',defaultValue: 'RunCukesIT', description: 'cucumber tests')
        //general
        string(name:'context',defaultValue: 'safebear', description: 'Application context')
        string(name:'domain',defaultValue: 'http://34.210.71.185', description: 'Application context')

        //test environment
        string(name:'test_hostname', defaultValue: '34.210.71.185', description:'hostname of the test environment')
        string(name:'test_port', defaultValue: '8888', description:'port of the test environment')
        string(name:'test_username', defaultValue: 'tomcat', description:'username of the test environment')
        string(name:'test_password', defaultValue: 'tomcat', description:'password of the test environment')

        string(name: 'browser', defaultValue: 'headless', description: 'browser for gui tests')
    }
    options{
        //this allows us to oly keep the artifacts and build logs of the last 3 builds
        buildDiscarder(logRotator(numToKeepStr: '3', artifactNumToKeepStr: '3'))
    }
    //poll every minute
    triggers{

        pollSCM('* * * * *' ) //poll the source code repo every minute
    }
    stages {
        stage('Build with Unit Testing') {
            steps {
                sh 'mvn clean package'
            }
            post {
                // only run if the last step succeeds
                success {
                    echo 'Now archiving.....'
                    //archive the artifacts so that we can build nce and then use them later
                    archiveArtifacts artifacts: '**/target/*.war'
                }
                always {
                    junit "**/target/surefire-reports/*.xml"
                }
            }
        }
        stage('Static Analysis') {
            steps {
                sh 'mvn checkstyle:checkstyle'
            }
            post {
                success {
                    checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''
                }
            }
        }
        stage('Deploy To Test') {
//            deploy to test so we can run our intergration and BDD tests
            steps {
                sh "mvn cargo:redeploy -Dcargo.hostname=${params.test_hostname} -Dcargo.servlet.port=${params.test_port} -Dcargo.username=${params.test_username} -Dcargo.password=${params.test_password} "
            }
        }

        // int test
        stage('Intergration Tests') {
            steps {
                sh "mvn -X -Dtest=${params.apiTests} test  -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}"

            }
            post {
                always {
                    junit "**/target/surefire-reports/*ApiTestsIT.xml"
                }
            }
        }

        stage('BDD Tests') {
            steps {
                sh "mvn -Dtest=${params.cuke} clean test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context} -Dbrowser=${params.browser}"
            }
            post {
                always {
                    publishHTML([
                            allowMissing         : false,
                            alwaysLinkToLastBuild: false,
                            keepAll              : false,
                            reportDir            : 'target/cucumber',
                            reportFiles          : 'index.html',
                            reportName           : 'BDD Report',
                            reportTitles         : ''
                    ])
                }
            }
        }

    }

    }
